﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Faction
{
    /// <summary>
    /// 
    /// </summary>
    public class NOD : FactionBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serverInfo"></param>
        public NOD(Server.ServerInfo serverInfo) : base(serverInfo)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override Base.Faction ID
        {
            get { return Base.Faction.NOD; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int ConstructionYardID
        {
            get { return (int) Base.Units.NOD_Construction_Yard; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit ConstructionYard
        {
            get { return _si.GetUnit(Base.Units.NOD_Construction_Yard); }
        }

        public override int BarracksID
        {
            get { return (int)Base.Units.NOD_Barracks; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Barracks
        {
            get { return _si.GetUnit(Base.Units.NOD_Barracks); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int FactoryID
        {
            get { return (int)Base.Units.NOD_Factory; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Factory
        {
            get { return _si.GetUnit(Base.Units.NOD_Factory); }
        }

        /// <summary>
        /// 
        /// </summary>
        public override int AirfieldID
        {
            get { return (int)Base.Units.NOD_Airport; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override GameData.Unit Airfield
        {
            get { return _si.GetUnit(Base.Units.NOD_Airport); }
        }
    }
}
