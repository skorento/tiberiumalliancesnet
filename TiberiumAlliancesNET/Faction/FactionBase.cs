﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Faction
{
    public abstract class FactionBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected Server.ServerInfo _si;

        /// <summary>
        /// 
        /// </summary>
        public FactionBase(Server.ServerInfo serverInfo)
        {
            _si = serverInfo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract Base.Faction ID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract int ConstructionYardID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract GameData.Unit ConstructionYard { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract int BarracksID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract GameData.Unit Barracks { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract int FactoryID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract GameData.Unit Factory { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract int AirfieldID { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public abstract GameData.Unit Airfield { get; }
    }
}
