﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Base
{
    public enum MovementType
    {
        None,
        Feet,
        Wheel,
        Track,
        Air,
        Air2,
        Unk
    }
}
