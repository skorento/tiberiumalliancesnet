﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Base
{
    public enum ResourceType
    {
        Unknown,
        Tiberium,
        Crystal,
        Credits,
        PlayerLevel,
        Power,
        ResearchPoints,
        RepairChargeBase,
        RepairChargeAir,
        RepairChargeInf,
        RepairChargeVeh,
        RepairChargeOffense_onlyReward,
        ZZ_UNUSED,
        ExperiencePoints,
        CommandPoints = 15,
        SupplyPoints,
        PackageProduction
    }
}