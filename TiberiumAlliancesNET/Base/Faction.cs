﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Base
{
    /// <summary>
    /// 
    /// </summary>
    public enum Faction
    {
        None,
        GDI,
        NOD,
        Forgotten
    }
}
