﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Base
{
    public enum EntityState
    {
        Normal,
        Damaged,
        Destroyed
    }
}
