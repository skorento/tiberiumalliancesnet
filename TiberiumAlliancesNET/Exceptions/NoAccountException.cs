﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Exceptions
{
    class NoAccountException : ServerException
    {
        /// <summary>
        /// 
        /// </summary>
        public NoAccountException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}