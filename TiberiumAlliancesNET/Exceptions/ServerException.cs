﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    class ServerException : System.Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public ServerException()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public ServerException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ServerException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}