﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
    class GeneralException : System.Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public GeneralException()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public GeneralException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public GeneralException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}