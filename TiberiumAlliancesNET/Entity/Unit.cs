﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Entity
{
    /// <summary>
    /// Instance of Unit
    /// </summary>
    public class Unit
    {
        /// <summary>
        /// 
        /// </summary>
        Server.ServerInfo _si;

        /// <summary>
        /// 
        /// </summary>
        public Base.Units UnitType
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public GameData.Unit UnitData
        {
            get
            {
                return _si.GetUnit(UnitType);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Level
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public double Health
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <todo>Calculate real value</todo>
        public double MaxHealth
        {
            get { return 100.0; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="si"></param>
        /// <param name="Unit"></param>
        /// <param name="Level"></param>
        /// <param name="Health"></param>
        public Unit(TiberiumAlliances.Server.ServerInfo si, TiberiumAlliances.Base.Units UnitType, int Level, double Health = -1)
        {
            _si = si;
            this.Level = Level;
            this.UnitType = UnitType;
            this.Health = (Health == -1 ? MaxHealth : Health);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildingLevel"></param>
        /// <returns></returns>
        public TimeSpan RepairTime(int buildingLevel = 12)
        {
            TimeSpan fullRepair = _si.GetUnit(UnitType).getRepairTime(Level, buildingLevel);
            double healthPercent = Health / MaxHealth;
            double actualRepairTime = fullRepair.TotalSeconds * (1 - healthPercent);

            return new TimeSpan((long)(actualRepairTime * TimeSpan.TicksPerSecond));
        }
    }
}
