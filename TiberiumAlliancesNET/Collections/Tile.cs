﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Collections
{
    public abstract class Tile
    {
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public int X { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public int Y { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>
        public Tile()
        {
        }
    }
}
