﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Collections
{
    namespace TileMap
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>
        public abstract class TileMap<T> where T : Tile
        {
            /// <summary>
            /// 
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public int Width { get; set; }
            /// <summary>
            /// 
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public int Height { get; set; }
            /// <summary>
            /// 
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public T[] Tiles { get; set; }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public T this[int x, int y]
            {
                get
                {
                    if (x < 0 | y < 0 | x >= Width | y >= Height)
                        throw new IndexOutOfRangeException();

                    return Tiles[getIndex(x, y)];
                }
                protected set
                {
                    value.X = x;
                    value.Y = y;

                    Tiles[getIndex(x, y)] = value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <remarks></remarks>
            protected TileMap()
            {
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="width"></param>
            /// <param name="height"></param>
            /// <remarks></remarks>
            public TileMap(int width, int height)
            {
                this.Width = width;
                this.Height = height;

                InitMap();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected int getIndex(int x, int y)
            {
                return x + (Width * y);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            /// <remarks></remarks>
            public T[] getNeigbours(int x, int y)
            {
                List<T> l = new List<T>();

                int[,] pos = {
				    {
					    x - 1,
					    y - 1
				    },
				    {
					    x,
					    y - 1
				    },
				    {
					    x + 1,
					    y - 1
				    },
				    {
					    x - 1,
					    y
				    },
				    {
					    x + 1,
					    y
				    },
				    {
					    x - 1,
					    y + 1
				    },
				    {
					    x,
					    y + 1
				    },
				    {
					    x + 1,
					    y + 1
				    }
			    };

                for (int i = 0; i <= pos.GetUpperBound(0); i++)
                {
                    if (pos[i, 0] >= 0 & pos[i, 1] >= 0 & pos[i, 0] < Width - 1 & pos[i, 1] < Height - 1)
                        l.Add(this[pos[i, 0], pos[i, 1]]);
                }

                return l.ToArray();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <remarks></remarks>
            protected abstract void InitMap();
        }
    }
}
