﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.WorldBrowser.Message
{
    /// <summary>
    /// 
    /// </summary>
    public class OriginAccountInfo
    {
        /// <summary>
        /// Account ID
        /// </summary>
        public int Account;

        /// <summary>
        /// 
        /// </summary>
        public List<Server> Servers;
    }
}
