﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.WorldBrowser
{
    public class PlayerDetails
    {
        /// <summary>
        /// 
        /// </summary>
        public int world;

        /// <summary>
        /// 
        /// </summary>
        public string name;

        /// <summary>
        /// 
        /// </summary>
        public int level;

        /// <summary>
        /// 
        /// </summary>
        public int points;

        /// <summary>
        /// 
        /// </summary>
        public int bases;

        /// <summary>
        /// 
        /// </summary>
        public int worldRank;

        /// <summary>
        /// 
        /// </summary>
        public string alliance;
    }
}
