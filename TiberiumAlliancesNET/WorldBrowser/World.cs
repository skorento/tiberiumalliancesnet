﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TiberiumAlliances.WorldBrowser
{
    /// <summary>
    /// 
    /// </summary>
    public class WorldBrowser
    {
        const string LOGIN_URL = "https://alliances.commandandconquer.com/j_security_check";
        const string WORLDBROWSER_URL = "https://alliances.commandandconquer.com/en/game/worldBrowser";

        const string WebWorldBrowser = "https://gamecdnorigin.alliances.commandandconquer.com/WebWorldBrowser/index.aspx";
        const string AJAX_ENDPOINT = "https://gamecdnorigin.alliances.commandandconquer.com/Farm/Service.svc/ajaxEndpoint/";

        /// <summary>
        /// 
        /// </summary>
        private Regex SessionID_RegEX = new Regex("name=\"sessionID\"\\s+value=\"([a-f0-9\\-]+)\"", RegexOptions.Compiled);

        /// <summary>
        /// 
        /// </summary>
        private Net.CookiesWebClient wc = new Net.CookiesWebClient();

        /// <summary>
        /// 
        /// </summary>
        public string SessionID
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Message.OriginAccountInfo AccountInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public WorldBrowser(string username, string password)
        {
            NameValueCollection postData = new NameValueCollection();

            postData["id"] = "";
            postData["j_password"] = password;
            postData["j_username"] = username;
            postData["spring-security-redirect"] = "";
            postData["timezone"] = "3";

            wc.UploadValues(LOGIN_URL, "POST", postData);
            string worldPage = wc.DownloadString(WORLDBROWSER_URL);

            Match sessionIDMatch = SessionID_RegEX.Match(worldPage);

            if (sessionIDMatch.Success)
                SessionID = sessionIDMatch.Groups[1].Value;
            else
                throw new Exceptions.ServerException("Wrong username or password!");

            AccountInfo = GetOriginAccountInfo();

            UpdateServerStatus();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionid"></param>
        public WorldBrowser(Guid SessionID) 
        {
            this.SessionID = SessionID.ToString("D");
            AccountInfo = GetOriginAccountInfo();

            UpdateServerStatus();
        }

        public WorldBrowser(string SessionID)
        {
            this.SessionID = SessionID;
            AccountInfo = GetOriginAccountInfo();

            UpdateServerStatus();
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateServerStatus()
        {
            List<int> servers = new List<int>();

            foreach (Server s in AccountInfo.Servers)
                servers.Add(s.Id);

            List<ServerStatus> statuses = GetServerStatus(servers);

            foreach (ServerStatus st in statuses)
            {
                foreach (Server s in AccountInfo.Servers)
                {
                    if (st.Id == s.Id)
                    {
                        s.PlayerCount = st.PlayerCount;
                        s.Online = st.Online;

                        if (s.Faction != 0)
                            s.PlayerDetails = GetPlayerDetails(s);

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Message.OriginAccountInfo GetOriginAccountInfo()
        {
            string response = _serverAjaxQuery("GetOriginAccountInfo");

            return JsonConvert.DeserializeObject<Message.OriginAccountInfo>(response);
        }

        /// <summary>
        /// 
        /// </summary>
        private List<ServerStatus> GetServerStatus(List<int> servers)
        {
            JObject obj = new JObject();

            obj.Add("ids", new JArray(servers));

            string response = _serverAjaxQuery("GetServerStatus", obj);

            return JsonConvert.DeserializeObject<List<ServerStatus>>(response);
        }

        /// <summary>
        /// Sends JSON query to sever and returns answer as string
        /// </summary>
        /// <param name="method"></param>
        /// <param name="obj"></param>
        private string _serverAjaxQuery(string method, JObject obj = null)
        {
            if (obj == null)
                obj = new JObject();

            obj.Add("session", SessionID);

            wc.RestoreHeaders();
            //wc.Headers.Add("Accept-Language: fi");
            wc.Headers.Add("Content-Type: application/json; charset=UTF-8");
            wc.Headers.Add(string.Format("Referer: {0}", WebWorldBrowser));
            wc.Headers.Add("X-Qooxdoo-Response-Type: application/json");

            Uri endPoint = new Uri(AJAX_ENDPOINT + method);

            return wc.UploadString(endPoint, "POST", obj.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private PlayerDetails GetPlayerDetails(Server server)
        {
            wc.RestoreHeaders();
            //wc.Headers.Add("Accept-Language: fi");
            //wc.Headers.Add("Content-Type: application/json; charset=UTF-8");
            wc.Headers.Add(string.Format("Referer: {0}", WebWorldBrowser));

            Uri endPoint = new Uri(
                server.Url + String.Format("/presentation/Service.svc/ajaxEndpoint/GetPlayerDetails?session={0}&player={1}", SessionID, AccountInfo.Account)
            );

            string reply = wc.DownloadString(endPoint);
            reply = reply.Substring(21, reply.Length - 22);

            return JsonConvert.DeserializeObject<PlayerDetails>(reply);
        }
    }
}
