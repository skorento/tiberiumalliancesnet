﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TiberiumAlliances.GameData
{
    /// <summary>
    /// 
    /// </summary>
    public class Tech
    {
        /// <summary>
        /// 
        /// </summary>
        public Server.ServerInfo Server
        {
            get;
            internal set;
        }

        /// <summary>
        /// Internal name
        /// </summary>
        /// <remarks></remarks>
        /// 
        public string n;

        /// <summary>
        /// Display Name
        /// </summary>
        /// <remarks></remarks>
        public string dn;

        public string dnuc;

        public string ds;

        public string sds;

        public int t;

        public int x;

        public int y;

        public List<StateImage> sic;

        public string di;

        public string ddi;

        public string ci;

        public string qi;

        public int e;

        public int ce;

        public int pe;

        public bool v;

        public int rp;

        public int s;

        public int sc;

        /// <summary>
        /// Max Count
        /// </summary>
        /// <remarks></remarks>
        public int mc;

        public int bf;
        /// <summary>
        /// Can demolish?
        /// </summary>
        /// <remarks></remarks>
        public bool da;

        // tp
        // trq

        /// <summary>
        /// Modifier Amounts
        /// </summary>
        /// <remarks></remarks>
        public List<TechCost> r;

        /// <summary>
        /// Faction
        /// </summary>
        /// <remarks></remarks>
        public Base.Faction f;

        /// <summary>
        /// Unit ID
        /// </summary>
        /// <remarks></remarks>
        public int c;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public TechCost getLevelCost(int level)
        {
            if ((r.Count - 1) >= level)
            {
                return r[level];
            }
            else
            {
                TechCost source = r[r.Count - 1];
                int levelsAboveMax = level - (r.Count - 1);

                TechCost lvCost = new TechCost();

                // ? cost
                if (source.rr.Count > 0)
                {
                    foreach (Cost.Cost<Base.ResourceType> rc in source.rr)
                    {
                        Cost.Cost<Base.ResourceType> newrc = (Cost.Cost<Base.ResourceType>) rc.Clone();
                        lvCost.rr.Add(newrc);

                        switch (newrc.t)
                        {
                            case Base.ResourceType.Tiberium:
                            case Base.ResourceType.Crystal:
                            case Base.ResourceType.Credits:
                            case Base.ResourceType.Power:
                                newrc.c = (long)Math.Floor((double)newrc.c * Math.Pow((double)Server.UnitLevelUpgradeFactorResource, levelsAboveMax));
                                break;
                            default:
                                
                                break;
                        }
                    }
                }

                // Repair Cost
                if (source.lm.Count > 0)
                {
                    foreach (Base.ModifierAmount rc in source.lm)
                    {
                        Base.ModifierAmount newrc = (Base.ModifierAmount)rc.Clone();
                        lvCost.lm.Add(newrc);

                        switch (newrc.t)
                        {
                            // Resource Production
                            case Base.ModifierType.TiberiumPerHour:
                            case Base.ModifierType.PowerPerhour:
                            case Base.ModifierType.CrystalPerHour:
                            case Base.ModifierType.CreditPerHour:
                                newrc.v = Math.Floor(newrc.v * Math.Pow(Server.TechLevelUpgradeFactorResourceProduction, levelsAboveMax));
                                break;

                            // Bonus Package Size
                            case Base.ModifierType.TiberiumPackageSize:
                            case Base.ModifierType.CrystalPackageSize:
                            case Base.ModifierType.PowerPackageSize:
                            case Base.ModifierType.CreditsBonusPackageSize:
                                newrc.v = Math.Floor(newrc.v * Math.Pow(Server.TechLevelUpgradeFactorBonusAmount, levelsAboveMax));
                                break;

                            // Bonus Package Time, not currently used anymore (TechLevelUpgradeFactorBonusTime = 1)
                            case Base.ModifierType.TiberiumBonusTime:
                            case Base.ModifierType.CrystalBonusTime:
                            case Base.ModifierType.PowerBonusTime:
                            case Base.ModifierType.CreditBonusTime:
                                newrc.v = Math.Floor(newrc.v * Math.Pow(Server.TechLevelUpgradeFactorBonusTime, levelsAboveMax));
                                break;

                            // Storage
                            case Base.ModifierType.TiberiumStorage:
                            case Base.ModifierType.CrystalStorage:
                            case Base.ModifierType.PowerStorage:
                                newrc.v = Math.Floor(newrc.v * Math.Pow(Server.TechLevelUpgradeFactorStorage, levelsAboveMax));
                                break;

                            // Army/Defense Points
                            case Base.ModifierType.ArmyPoints:
                                newrc.v = Math.Min(newrc.v + Server.GetArmyPoints(levelsAboveMax), Server.MaxArmyPoints);
                                break;
                            case Base.ModifierType.DefensePoints:
                                newrc.v = Math.Min(newrc.v + Server.GetDefensePoints(levelsAboveMax), Server.MaxDefensePoints);
                                break;

                            // Construction Slots
                            case Base.ModifierType.BuildingSlots:
                                newrc.v = Math.Min(newrc.v + levelsAboveMax, Server.MaxBuildingSlots);
                                break;

                            // Repair time boost
                            case Base.ModifierType.RepairEfficiencyAir:
                            case Base.ModifierType.RepairEfficiencyInf:
                            case Base.ModifierType.RepairEfficiencyVeh:
                                newrc.v = newrc.v * Math.Pow(Server.UnitRepairTimeEfficiencyFactor, levelsAboveMax);
                                break;
                            case Base.ModifierType.RepairEfficiencyBase:
                                newrc.v = newrc.v * Math.Pow(Server.BaseRepairTimeEfficiencyFactor, levelsAboveMax);
                                break;

                            // These are not affected by levels
                            case Base.ModifierType.RepairStorageBase:
                            case Base.ModifierType.RepairStorageOffense:
                            case Base.ModifierType.RepairProductionPerHourBase:
                            case Base.ModifierType.RepairProductionOffense:
                            case Base.ModifierType.SupportDamageAir:
                            case Base.ModifierType.SupportDamageInf:
                            case Base.ModifierType.SupportDamageVeh:
                            case Base.ModifierType.SupportPrepareTime:
                            case Base.ModifierType.SupportTimePerField:
                            case Base.ModifierType.SupportRadius:
                                break;

                            // Everything else uses generic modifier
                            default:
                                Debugger.Log(0, "Debug", newrc.t.ToString());
                                newrc.v = (long)Math.Floor(newrc.v * Math.Pow(Server.TechLevelUpgradeFactorValues, levelsAboveMax));
                                break;
                      }
                    }
                }

                return lvCost;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public double getModifierAtLevel(Base.ModifierType mt, int level)
        {
            TechCost cost = getLevelCost(level);

            foreach (Base.ModifierAmount rc in cost.lm)
            {
                if (rc.t == mt)
                    return rc.v;
            }

            throw new Exception("Not found");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return dn;
        }
    }
}