﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    /// <summary>
    /// 
    /// </summary>
    public class Costs
    {
        /// <summary>
        /// 
        /// </summary>
        public Cost.ResouceCostList r;
        /// <summary>
        /// 
        /// </summary>
        public Cost.ResouceCostList lr;
        /// <summary>
        /// Repair Cost
        /// </summary>
        public Cost.ResouceCostList rer;
        /// <summary>
        /// Upgrade Cost
        /// </summary>
        public Cost.ResouceCostList rr;

        /// <summary>
        /// 
        /// </summary>
        public Costs()
        {
            r = new Cost.ResouceCostList();
            lr = new Cost.ResouceCostList();
            rer = new Cost.ResouceCostList();
            rr = new Cost.ResouceCostList();
        }
    }
}
