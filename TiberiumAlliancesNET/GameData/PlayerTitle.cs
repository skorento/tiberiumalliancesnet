﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    public class PlayerTitle
    {
        /// <summary>
        /// Internal name
        /// </summary>
        /// <remarks></remarks>
        public string n;

        /// <summary>
        /// Display Name
        /// </summary>
        /// <remarks></remarks>
        public string dn;
        public string bic;
        public string ic;
        public string sic;

        /// <summary>
        /// Supplypoint generation
        /// </summary>
        /// <remarks></remarks>
        public int spg;

        /// <summary>
        /// Supplypoint max
        /// </summary>
        /// <remarks></remarks>
        public int sps;

        /// <summary>
        /// Points required
        /// </summary>
        /// <remarks></remarks>
        public int r;

        /// <summary>
        /// Level/Rank
        /// </summary>
        /// <remarks></remarks>
        public int t;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return dn;
        }
    }
}
