﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    /***
     * "81": {
            n: "GDI_Riflemen",
            dn: "Rifleman Squad",
            dnuc: "RIFLEMAN SQUAD",
            ds: "Basic squad, best suited to fight other infantry.",
            dimg: "battleground/gdi/units/Rflmn/RflMn_detail.png",
            simg: "battleground/gdi/units/Rflmn/RflMn_setup.png",
            iimg: "battleground/gdi/units/Rflmn/RflMn_icn.png",
            mi: "",
            ci: {
                defc: {
                    img: "battleground/gdi/units/Rflmn/RflMn.png",
                    e: 375
                },
                dmgc: null,
                dstc: null
            },
            ai: null,
            ce: [ [ 367, 367, 367, -1 ], [ -1, -1, -1, -1 ], [ -1, -1, -1, -1 ], [ -1, -1, -1, -1 ] ],
            de: 368,
            ge: [ 369, -1, -1 ],
            fe: {
                "0": 370
            },
            fes: {},
            ie: {},
            be: {
                "126": [ 371 ],
                "125": [ 372, 373, 374 ]
            },
            se: -1,
            dd: null,
            x: 0,
            y: 0,
            uc: 5,
            
     *      r: .....
    
            s: 60,
            lp: 700,
            f: 1,
            at: 1,
            ptt: 1,
            mt: 1,
            pt: 2,
            md: 0,
            cd: 110,
            m: [ {
                r: [],
                t: 1,
                tat: 1,
                i: 1,
                as: true,
                d: 220,
                rmax: 150,
                rmin: 0,
                ai: 17,
                du: 0,
                c: 0
            }, {
                r: [],
                t: 1,
                tat: 2,
                i: 2,
                as: false,
                d: 110,
                rmax: 150,
                rmin: 0,
                ai: 17,
                du: 0,
                c: 0
            }, {
                r: [],
                t: 1,
                tat: 3,
                i: 3,
                as: false,
                d: 0,
                rmax: 150,
                rmin: 0,
                ai: 17,
                du: 0,
                c: 0
            }, {
                r: [],
                t: 1,
                tat: 4,
                i: 4,
                as: false,
                d: 70,
                rmax: 150,
                rmin: 0,
                ai: 17,
                du: 0,
                c: 0
            }, {
                r: [],
                t: 1,
                tat: 5,
                i: 5,
                as: true,
                d: 70,
                rmax: 150,
                rmin: 0,
                ai: 4,
                du: 0,
                c: 0
            }, {
                r: [ {
                    i: 120,
                    l: 2,
                    a: 1
                } ],
                t: 3,
                tat: 4,
                i: 321,
                as: false,
                d: 0,
                rmax: 150,
                rmin: 0,
                ai: 44,
                du: 1e3,
                c: 500
            } ],
            b: {
                "1": {
                    e: 2,
                    f: 2
                },
                "2": {
                    e: 5,
                    f: 2
                },
                "3": {
                    e: 1,
                    f: 2
                },
                "4": {
                    e: 3,
                    f: 3
                },
                "5": {
                    e: 3,
                    f: 3
                }
            },
            bi: "battleground/gdi/units/Rflmn/RflMn.png",
            go: 60,
            p: false,
            tl: 120
     * 
     * 
     */

    public class Unit
    {
        /// <summary>
        /// 
        /// </summary>
        public Server.ServerInfo Server
        {
            get;
            internal set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int ID
        {
            get;
            internal set;
        }

        /// <summary>
        /// 
        /// </summary>
        internal int at;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<int, UnitBlockType> b;

        /// <summary>
        /// Internal name
        /// </summary>
        /// <remarks></remarks>
        public string n;

        /// <summary>
        /// Display Name
        /// </summary>
        /// <remarks></remarks>
        internal string dn;

        /// <summary>
        /// DisplayName Upper Case
        /// </summary>
        /// <remarks></remarks>
        public string dnuc;

        /// <summary>
        /// Description
        /// </summary>
        /// <remarks></remarks>
        public string ds;

        /// <summary>
        /// Tech ID
        /// </summary>
        public int tl;

        /// <summary>
        /// 
        /// </summary>
        public Tech Tech
        {
            get
            {
                return Server.GetTech(tl);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Base.PlacementType pt;

        /// <summary>
        /// 
        /// </summary>
        public Base.MovementType mt;

        /// <summary>
        /// Faction
        /// </summary>
        /// <remarks></remarks>
        public Base.Faction f;

        /// <summary>
        /// 
        /// </summary>
        public List<Costs> r;

        /// <summary>
        /// Hit Points
        /// </summary>
        public long lp;

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Base.UnitType UnitType
        {
            get
            {
                switch (mt)
                {
                    case Base.MovementType.Feet:
                        return Base.UnitType.Infrantry;
                    case Base.MovementType.Track:
                    case Base.MovementType.Wheel:
                        return Base.UnitType.Vehicle;
                    case Base.MovementType.Air:
                    case Base.MovementType.Air2:
                        return Base.UnitType.Air;
                    default:
                        return Base.UnitType.Building; // Is this correct?
                        throw new Exception("Unknown Unit Type");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Base.ResourceType RepairType
        {
            get
            {
                switch (UnitType)
                {
                    case Base.UnitType.Air:
                        return Base.ResourceType.RepairChargeAir;
                    case Base.UnitType.Building:
                        return Base.ResourceType.RepairChargeBase;
                    case Base.UnitType.Infrantry:
                        return Base.ResourceType.RepairChargeInf;
                    case Base.UnitType.Vehicle:
                        return Base.ResourceType.RepairChargeVeh;
                    default:
                        throw new Exception("Unknown Unit Type");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public Costs getLevelCost(int level)
        {
            if (level < 1)
                throw new ArgumentException("Must be greater than one", "level");

            if ((r.Count - 1) >= level)
            {
                return r[level];
            }
            else
            {
                Costs source = r[r.Count - 1];
                int levelsAboveMax = level - (r.Count - 1);

                Costs lvCost = new Costs();

                // Upgrade cost
                if (source.rr.Count > 0)
                {
                    foreach (Cost.Cost<Base.ResourceType> rc in source.rr)
                    {
                        Cost.Cost<Base.ResourceType> newrc = (Cost.Cost<Base.ResourceType>)rc.Clone();
                        lvCost.rr.Add(newrc);

                        switch (newrc.t)
                        {
                            case Base.ResourceType.Tiberium:
                            case Base.ResourceType.Crystal:
                            case Base.ResourceType.Credits:
                            case Base.ResourceType.Power:
                                newrc.c = (long)Math.Floor((double)newrc.c * Math.Pow((double)Server.UnitLevelUpgradeFactorResource, levelsAboveMax));
                                break;
                            default:
                                break;
                        }
                    }
                }

                // Repair Cost
                if (source.rer.Count > 0)
                {
                    foreach (Cost.Cost<Base.ResourceType> rc in source.rer)
                    {
                        Cost.Cost<Base.ResourceType> newrc = (Cost.Cost<Base.ResourceType>)rc.Clone();
                        lvCost.rer.Add(newrc);

                        switch (newrc.t)
                        {
                            case Base.ResourceType.Tiberium:
                            case Base.ResourceType.Crystal:
                            case Base.ResourceType.Credits:
                            case Base.ResourceType.Power:
                                newrc.c = (long)Math.Floor((double)newrc.c * Math.Pow((double)Server.UnitLevelUpgradeFactorRepairResource, levelsAboveMax));
                                break;
                            case Base.ResourceType.ResearchPoints:
                                newrc.c = (long)Math.Floor((double)newrc.c * Math.Pow((double)Server.UnitLevelUpgradeFactorResource, levelsAboveMax));
                                break;
                            case Base.ResourceType.RepairChargeBase:
                                newrc.c = (long) Math.Floor((double) newrc.c * Server.GetBaseUpgradeFactorRepairTime(levelsAboveMax));
                                break;
                            case Base.ResourceType.RepairChargeAir:
                            case Base.ResourceType.RepairChargeInf:
                            case Base.ResourceType.RepairChargeVeh:
                                newrc.c = (long)Math.Floor((double)newrc.c * Math.Pow((double)Server.UnitRepairTimeFactor, levelsAboveMax));
                                break;
                            default:
                                break;
                        }
                    }
                }

                return lvCost;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="buildingLevel"></param>
        /// <returns></returns>
        public TimeSpan getRepairTime(int level, int buildingLevel = 12)
        {
            double repairEff = 100;
 
            if (buildingLevel != 12)
            {
                Unit repairBuilding;

                switch (RepairType)
                {
                    case Base.ResourceType.RepairChargeBase:
                        repairBuilding = Server.GetUnit(Server.GetFaction(f).ConstructionYardID);
                        repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyBase, buildingLevel);
                        break;
                    case Base.ResourceType.RepairChargeAir:
                        repairBuilding = Server.GetUnit(Server.GetFaction(f).AirfieldID);
                        repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyAir, buildingLevel);
                        break;
                    case Base.ResourceType.RepairChargeInf:
                        repairBuilding = Server.GetUnit(Server.GetFaction(f).BarracksID);
                        repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyInf, buildingLevel);
                        break;
                    case Base.ResourceType.RepairChargeVeh:
                        repairBuilding = Server.GetUnit(Server.GetFaction(f).FactoryID);
                        repairEff = repairBuilding.Tech.getModifierAtLevel(Base.ModifierType.RepairEfficiencyVeh, buildingLevel);
                        break;
                    default:
                        throw new Exception("Unknown RepairType");
                }
            }

            Costs cost = getLevelCost(level);

            foreach (Cost.Cost<Base.ResourceType> rc in cost.rer)
            {
                if (rc.t == RepairType)
                {
                    double repairTime = (rc.c / (repairEff / 100.0));
                    TimeSpan ts = new TimeSpan((long)(repairTime * TimeSpan.TicksPerSecond));
                    return ts;
                }
            }

            throw new Exception("???");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return dn;
        }
    }
}