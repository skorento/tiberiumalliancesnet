﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.GameData
{
    public class NerfModifier
    {
        /// <summary>
        /// Combat
        /// </summary>
        public double c;

        /// <summary>
        /// Economy
        /// </summary>
        public double e;
    }
}
