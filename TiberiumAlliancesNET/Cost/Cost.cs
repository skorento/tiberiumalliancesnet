﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Cost
{
    /// <summary>
    /// 
    /// </summary>
    public class Cost<T> : ICloneable
    {
        /// <summary>
        /// 
        /// </summary>
        public T t;

        /// <summary>
        /// 
        /// </summary>
        public long c;

        /// <summary>
        /// 
        /// </summary>
        public Cost()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Cost(T type, long cost)
        {
            t = type;
            c = cost;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (obj as Cost<T>).t.Equals(t) && (obj as Cost<T>).c == c;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return t.GetHashCode() ^ c.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0} {1}", c, t);
        }
    }
}