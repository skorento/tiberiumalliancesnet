﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Cost
{
    /// <summary>
    /// 
    /// </summary>
    public class RepairCost
    {
        /// <summary>
        /// 
        /// </summary>
        public Cost.ResouceCostList Cost
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan Infrantry
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan Vehicle
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan Air
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="infantry"></param>
        /// <param name="vehicle"></param>
        /// <param name="air"></param>
        public RepairCost(Cost.ResouceCostList Cost, TimeSpan infantry, TimeSpan vehicle, TimeSpan air)
        {
            this.Cost = Cost;
            this.Infrantry = infantry;
            this.Vehicle = vehicle;
            this.Air = air;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("Crystal: {3} Infranty: {0} Vehicle: {1} Air: {2}", Infrantry, Vehicle, Air, Cost[Base.ResourceType.Crystal].c);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(RepairCost))
            {
                // Equal if all times and costs are same
                return
                    (obj as RepairCost).Infrantry == this.Infrantry &&
                    (obj as RepairCost).Vehicle == this.Vehicle &&
                    (obj as RepairCost).Air == this.Air &&
                    Cost.SequenceEqual((obj as RepairCost).Cost);
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Cost.GetHashCode() ^ Infrantry.GetHashCode() ^ Vehicle.GetHashCode() ^ Air.GetHashCode();
        }
    }
}
