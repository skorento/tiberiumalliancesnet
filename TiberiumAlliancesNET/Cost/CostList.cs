﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Cost
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    abstract public class CostList<T> : List<Cost<T>>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Cost<T> this[T type]
        {
            get
            {
                foreach (Cost<T> c in this)
                {
                    if (Comparer<T>.Equals(type, c.t))
                        return c;
                }

                // If not found return new
                Cost<T> cost = createCost(type);
                Add(cost);

                return cost;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract Cost<T> createCost(T type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj.GetType() == this.GetType())
                return this.SequenceEqual((CostList<T>) obj);
            else
                return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            int hashCode = 0;

            foreach (Cost<T> c in this)
                hashCode = hashCode ^ c.GetHashCode();

            return hashCode;
        }
    }
}
