﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace TiberiumAlliances.Server
{
    /// <summary>
    /// Used for connection to game server
    /// </summary>
    public class ServerConnector
    {
        /// <summary>
        /// 
        /// </summary>
        private Uri host;

        /// <summary>
        /// 
        /// </summary>
        private string sessionID = "";

        /// <summary>
        /// 
        /// </summary>
        private Net.CookiesWebClient wc = new Net.CookiesWebClient();

        /// <summary>
        /// 
        /// </summary>
        private int ClientVersion = -1;

        /// <summary>
        /// 
        /// </summary>
        private bool HasConnection = false;

        /// <summary>
        /// 
        /// </summary>
        private int RequestID = 0;

        /// <summary>
        /// 
        /// </summary>
        private int SequenceID = 0;

        /// <summary>
        /// 
        /// </summary>
        public int version
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string language
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Message.PlayerInfo Player
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ServerConnector(Uri url, string session)
        {
            host = url;
            sessionID = session;

            _loadMainPage();

            Player = GetPlayerInfo();
        }

        /// <summary>
        /// 
        /// </summary>
        public ServerConnector(WorldBrowser.WorldBrowser world, WorldBrowser.Server server)
        {
            if (server.Faction == 0)
                throw new Exception("No account on this server!");

            host = new Uri(server.Url + "/");
            _loadMainPage();

            OpenSession(world);

            Player = GetPlayerInfo();
        }

        /// <summary>
        /// 
        /// </summary>
        private void _loadMainPage()
        {
            wc.RestoreHeaders();
            wc.Headers.Add(string.Format("Referer: {0}", new Uri(host, "index.aspx")));

            string mainPage = wc.DownloadString(new Uri(host, "index.aspx"));

            Regex chNum = new Regex("PerforceChangelist = (\\d+)(.+)Language = \"([a-z]{2})\"", RegexOptions.Singleline);
            
            Match match = chNum.Match(mainPage);

            version = int.Parse(match.Groups[1].Value);
            language = match.Groups[3].Value;
        }

        /// <summary>
        /// 
        /// </summary>
        public Message.OpenSession OpenSession(WorldBrowser.WorldBrowser world)
        {
            JObject q = new JObject();

            q.Add("session", world.SessionID);
            q.Add("reset", true);
            q.Add("refId", DateTime.UtcNow.ToJavaScriptMilliseconds());
            q.Add("version", ClientVersion);

            Message.OpenSession os = JsonConvert.DeserializeObject<Message.OpenSession>(_serverAjaxQuery("OpenSession", q));

            if (os.r < 0)
                throw new Exception(string.Format("OpenSession error: {0}", os.r));

            sessionID = os.i;

            HasConnection = true;

            RequestID = 0;
            SequenceID = 0;

            return os;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Message.PlayerInfo GetPlayerInfo()
        {
            return JsonConvert.DeserializeObject<Message.PlayerInfo>(_serverAjaxQuery("GetPlayerInfo"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Message.PublicPlayerInfo GetPublicPlayerInfo(int id)
        {
            JObject obj = new JObject();
            obj.Add("id", id);
            return JsonConvert.DeserializeObject<Message.PublicPlayerInfo>(_serverAjaxQuery("GetPublicPlayerInfo", obj));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Message.PublicAllianceInfo GetPublicAllianceInfo(int id)
        {
            JObject obj = new JObject();
            obj.Add("id", id);
            return JsonConvert.DeserializeObject<Message.PublicAllianceInfo>(_serverAjaxQuery("GetPublicAllianceInfo", obj));
        }

        /// <summary>
        /// 
        /// </summary>
        private void SendPoll(Dictionary<string, string> requests)
        {
            string requests_str = "UA\\\\f";


            foreach (KeyValuePair<string, string> p in requests)
                requests_str += string.Format("{0}:{1}\\\\f", p.Key, p.Value);

            JObject q = new JObject();
            q.Add("session", sessionID);
            q.Add("sequenceid", SequenceID.ToString());
            q.Add("requestid", RequestID.ToString());
            q.Add("requests", requests_str);

            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal Message.ServerInfo GetServerInfo()
        {
            return JsonConvert.DeserializeObject<Message.ServerInfo>(_serverAjaxQuery("GetServerInfo"));
        }

        /// <summary>
        /// Saves json data and script from server to files
        /// </summary>
        public void dumpJS(string dir)
        {
            string js = wc.DownloadString(new Uri(host, string.Format("{0}/data_{1}.html", version, language)));

            // Parse all javascript data, which is split into parts
            // probably to show load indicator
            Regex rg = new Regex("<script type=\"text/javascript\">s\\(\"(.+?)\"\\);</script>");

            string parsedJS = "";

            foreach (Match m in rg.Matches(js))
                parsedJS += m.Groups[1].Value;

            js = ""; // use less memory ;)

            // Unescape javascript
            parsedJS = parsedJS.
                Replace("\\\"", "\"").Replace("\\r\\n", "\n").Replace("\\n", "\n").
                Replace("\\t", "\t").Replace("\\\n", "\\n").Replace("\\\t", "\\t").Replace("\\\\", "\\");

            // Match GAMEDATA and files from parsed jaavscript 
            Regex rgGD = new Regex("GAMEDATA=\\{(.+?)\\};", RegexOptions.Singleline);
            Regex rgFiles = new Regex("FILES\\s?=\\s?\\{(.+?)\\};", RegexOptions.Singleline);

            string gamedata = "{" + rgGD.Match(parsedJS).Groups[1].Value + "}";
            string files = "{" + rgFiles.Match(parsedJS).Groups[1].Value + "}";

            // Replace gamedata and files with nothing.
            parsedJS = rgGD.Replace(parsedJS, "");
            parsedJS = rgFiles.Replace(parsedJS, "");

            System.IO.TextWriter tw = new System.IO.StreamWriter(dir + "/gamedata.json");
            tw.Write(gamedata);
            tw.Close();
            tw.Dispose();

            tw = new System.IO.StreamWriter(dir + "/files.json");
            tw.Write(files);
            tw.Close();
            tw.Dispose();

            tw = new System.IO.StreamWriter(dir + "/script.js");
            tw.Write(parsedJS);
            tw.Close();
            tw.Dispose();
        }

        /// <summary>
        /// Sends JSON query to sever and returns answer as string
        /// </summary>
        /// <param name="method"></param>
        /// <param name="obj"></param>
        private string _serverAjaxQuery(string method, JObject obj = null)
        {
            if (obj == null)
                obj = new JObject();

            if (sessionID != null && sessionID != "")
                obj.Add("session", sessionID);

            wc.RestoreHeaders();
            wc.Headers.Add("Content-Type: application/json; charset=UTF-8");
            wc.Headers.Add(string.Format("Referer: {0}", new Uri(host, "index.aspx")));
            wc.Headers.Add("X-Qooxdoo-Response-Type: application/json");

            Uri endPoint = new Uri(host, "Presentation/Service.svc/ajaxEndpoint/" + method);

            string reply = wc.UploadString(endPoint, "POST", obj.ToString());

            if (reply == "")
            { 
            
            }

            return reply;
        }
    }
}
