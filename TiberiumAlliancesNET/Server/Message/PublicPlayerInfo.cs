﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiberiumAlliances.Server.Message
{
    /// <summary>
    /// 
    /// </summary>
    public class PublicPlayerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public class City
        {
            /// <summary>
            /// ID?
            /// </summary>
            public int i;

            /// <summary>
            /// Name
            /// </summary>
            public string n;

            /// <summary>
            /// Points
            /// </summary>
            public int p;

            /// <summary>
            /// 
            /// </summary>
            public int x;

            /// <summary>
            /// 
            /// </summary>
            public int y;
        }

        /// <summary>
        /// Alliance ID
        /// </summary>
        public int a;

        /// <summary>
        /// Alliance Name
        /// </summary>
        public string an;

        /// <summary>
        /// 
        /// </summary>
        public int bd;

        /// <summary>
        /// 
        /// </summary>
        public int bde;

        /// <summary>
        /// Cities
        /// </summary>
        public List<City> c;

        /// <summary>
        /// 
        /// </summary>
        public int d;

        /// <summary>
        /// Distance to center
        /// </summary>
        public double dccc;

        /// <summary>
        /// Faction
        /// </summary>
        public Base.Faction f;

        /// <summary>
        /// ID?
        /// </summary>
        public int i;

        /// <summary>
        /// Name
        /// </summary>
        public string n;

        /// <summary>
        /// Points
        /// </summary>
        public int p;

        /// <summary>
        /// Ranking
        /// </summary>
        public int r;
    }
}
